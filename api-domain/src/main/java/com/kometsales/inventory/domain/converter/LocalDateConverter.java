package com.kometsales.inventory.domain.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateConverter extends AbstractBeanField {

    @Override
    protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        try {
            LocalDate parse = LocalDate.parse(s.trim(), DateTimeFormatter.ofPattern("uuuu-MM-dd"));
            return parse;
        } catch (DateTimeParseException e) {
            throw new CsvDataTypeMismatchException(e.getMessage());
        }
    }
}
