package com.kometsales.inventory.service;

import com.kometsales.inventory.domain.exception.BusinessException;
import com.kometsales.inventory.domain.model.Product;
import com.kometsales.inventory.repository.ProductRepository;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
    private ProductRepository productRepository;
    private MailService mailService;

    @Autowired
    public ProductService(ProductRepository productRepository, MailService mailService) {
        this.productRepository = productRepository;
        this.mailService = mailService;
    }

    public Page<Product> getProduct(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public Optional<Product> getProduct(String code) {
        return productRepository.findByCode(code);
    }

    public void deleteProduct(String code) {
        final Optional<Product> optionalProduct = this.getProduct(code);
        optionalProduct.ifPresent(product -> {
            productRepository.delete(product);
            LOGGER.info("Product deleted with code:" + code);
        });
    }

    public Optional<Product> patchProduct(String code, int amount, double cost) {
        final Optional<Product> optionalProduct = this.getProduct(code);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            product.setAmount(amount);
            product.setCost(cost);
            productRepository.save(product);
            LOGGER.info("Product patched with code:" + code);
        }
        return optionalProduct;
    }

    @Async
    @SuppressWarnings(value = "unchecked")
    public void processingUploadFile(String emailForNotification, String fileName, InputStream fileInputStream) throws BusinessException {

        LOGGER.info("starting to process input file");
        try {
            CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(fileInputStream))
                    .withCSVParser(new CSVParserBuilder().withSeparator(',').build())
                    .build();

            CsvToBean<Product> csvToBean= new CsvToBeanBuilder(csvReader)
                    .withType(Product.class)
                    .withThrowExceptions(false)
                    .withIgnoreLeadingWhiteSpace(false)
                    .build();

            final List<Product> products = reduceProducts(csvToBean.parse());
            this.saveOrUpdateInventory(products);
            LOGGER.info("finished process input file");

            mailService.sendEmailNotification(emailForNotification, fileName, products.size()
                    , csvToBean.getCapturedExceptions().size()
                    , products.size() + csvToBean.getCapturedExceptions().size()
                    , csvToBean.getCapturedExceptions());

            LOGGER.info("Email sent successfully");
        } catch (Exception e) {
            throw new BusinessException("Error when processing csv: " + e.getMessage());
        }
    }

    private void saveOrUpdateInventory(List<Product> products) {

        products.forEach(productToSave -> productRepository.save(productRepository.findByCode(productToSave.getCode())
                .map(storedProduct -> merge(storedProduct, productToSave))
                .orElse(productToSave)));

    }

    private List<Product> reduceProducts(List<Product> products) {

        final Map<String, Optional<Product>> collect = products.stream()
                .collect(Collectors.groupingBy(Product::getCode, Collectors.reducing(this::merge)));

        return collect.values().stream().map(Optional::get).collect(Collectors.toList());
    }

    private Product merge(Product source, Product dest) {
        double operationalCost = source.getAmount() * source.getCost();
        double newOperationalCost = dest.getAmount() * dest.getCost();
        source.setAmount(source.getAmount() + dest.getAmount());
        source.setCost((newOperationalCost + operationalCost) / source.getAmount());
        return source;
    }
}
