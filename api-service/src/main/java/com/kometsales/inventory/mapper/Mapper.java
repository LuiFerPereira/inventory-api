package com.kometsales.inventory.mapper;

import com.kometsales.inventory.contract.dto.ProductDTO;
import com.kometsales.inventory.domain.model.Product;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class Mapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Product.class, ProductDTO.class).byDefault().register();
    }
}
