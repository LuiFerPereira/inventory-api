package com.kometsales.inventory.contract.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductTypeDTO {

    TYPE_1(1, 2.1),
    TYPE_2(2, 0.7),
    TYPE_3(3, 3.3),
    TYPE_4(4, 5.8);

    private int value;
    private double margin;

    ProductTypeDTO(int value, double margin) {
        this.value = value;
        this.margin = margin;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ProductTypeDTO fromValue(String text) throws Exception {
        for (ProductTypeDTO productType: ProductTypeDTO.values()) {
            if (String.valueOf(productType.value).equals(text)) {
                return productType;
            }
        }
        throw new Exception("Error creating ProductTypeDTO from text: " + text);
    }
}
