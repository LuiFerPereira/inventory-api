package com.kometsales.inventory.domain.model;

import com.kometsales.inventory.domain.converter.*;
import com.opencsv.bean.CsvCustomBindByName;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(indexes = {@Index(name = "code_idx",  columnList="code", unique = true)})
public class Product implements ProductInventory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CsvCustomBindByName(column = "code", converter = StringValueConverter.class, required = true)
    private String code;

    @CsvCustomBindByName(column = "name", converter = StringValueConverter.class, required = true)
    private String productName;

    @CsvCustomBindByName(column = "type", converter = ProductTypeValueConverter.class, required = true)
    private ProductType productType;

    @CsvCustomBindByName(column = "amount", converter = IntegerValueConverter.class, required = true)
    private int amount;

    @Column(precision = 16, scale = 2)
    @CsvCustomBindByName(column = "cost", converter = DoubleValueConverter.class, required = true)
    private double cost;

    @CsvCustomBindByName(column = "date", converter = LocalDateConverter.class, required = true)
    private LocalDate availabilityDate;

    public Product() {
    }

    public Product(String code, String productName, ProductType productType, int amount, double cost, LocalDate availabilityDate) {
        this.code = code;
        this.productName = productName;
        this.productType = productType;
        this.amount = amount;
        this.cost = cost;
        this.availabilityDate = availabilityDate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public BigDecimal getPrice() {
        return BigDecimal.valueOf(this.getCost() + calculateIva() + calculateProfit());
    }

    private double calculateIva() {
        return (this.cost * this.IVA_PERCENT) / 100;
    }

    private double calculateProfit() {
        return (this.cost * this.getProductType().getMargin()) / 100;
    }


    public LocalDate getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", productName='" + productName + '\'' +
                ", productType=" + productType +
                ", amount=" + amount +
                ", cost=" + cost +
                ", price=" + this.getPrice().doubleValue() +
                ", availabilityDate=" + availabilityDate +
                '}';
    }
}
