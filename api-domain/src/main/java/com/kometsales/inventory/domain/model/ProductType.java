package com.kometsales.inventory.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductType {

    TYPE_1(1, 12),
    TYPE_2(2, 30.5),
    TYPE_3(3, 8.95),
    TYPE_4(4, 10.33);

    private int value;
    private double margin;

    ProductType(int value, double margin) {
        this.value = value;
        this.margin = margin;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public double getMargin() {
        return margin;
    }

    public void setMargin(double margin) {
        this.margin = margin;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ProductType fromValue(String text) throws Exception {
        for (ProductType productType: ProductType.values()) {
            if (String.valueOf(productType.value).equals(text)) {
                return productType;
            }
        }
        throw new Exception("Error creating ProductType from text: " + text);
    }
}
