package com.kometsales.inventory.domain.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class IntegerValueConverter extends AbstractBeanField {

    private final int MAX_AMOUNT_VALUE = 1000000;

    @Override
    protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        try {
            final int value = Integer.parseInt(s.trim());
            if (value < 0 || value > MAX_AMOUNT_VALUE) {
                throw new CsvConstraintViolationException("Invalid input, must be required a number between [1,"+ MAX_AMOUNT_VALUE+"]");
            }
            return value;
        } catch (NumberFormatException e) {
            throw new CsvDataTypeMismatchException(e.getMessage());
        }
    }
}
