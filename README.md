**Developer**: Luis Fernando Pereira  

**Email**: lyufer@gmail.com  

## Requirements
  
Java 8, Maven 3.1+, MySQL 5+  
Default configuration:  
database name: _inventory_  
database port: 3306 (default)

## Build

`mvn clean install`

## Run:

**With Maven**  
  
`mvn exec:java -pl api-service -Dexec.mainClass=com.kometsales.inventory.Application`  

**With Java**  

`java -jar api-service/target/api-service-1.0.0.jar`


## Endpoint documentation Swagger 

`http://localhost:8080/api/inventory/swagger-ui.html`

[Swagger-UI](http://localhost:8080/api/inventory/swagger-ui.html)