package com.kometsales.inventory.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncExceptionHandler.class);

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Exception message - ")
                .append(throwable.getMessage())
                .append(" Method name - ")
                .append(method.getName());
        for (Object param : obj) {
            stringBuilder.append("Parameter value - ").append(param);
        }

        LOGGER.error("AsyncUncaughtException when processing file. {} ", stringBuilder.toString());
    }

}
