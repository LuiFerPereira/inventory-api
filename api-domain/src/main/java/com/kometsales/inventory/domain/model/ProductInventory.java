package com.kometsales.inventory.domain.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kometsales.inventory.domain.serializer.BigDecimalSerializer;

import java.math.BigDecimal;

public interface ProductInventory {

    double IVA_PERCENT = 19;

    @JsonSerialize(using = BigDecimalSerializer.class)
    BigDecimal getPrice();
}
