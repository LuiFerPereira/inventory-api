package com.kometsales.inventory.controller;

import com.google.common.base.Preconditions;
import com.kometsales.inventory.contract.dto.ProductDTO;
import com.kometsales.inventory.domain.model.Product;
import com.kometsales.inventory.service.ProductService;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController()
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
    private ProductService productService;
    private MapperFacade mapper;

    @Autowired
    public ProductController(ProductService productService, MapperFacade mapper) {
        this.productService = productService;
        this.mapper = mapper;
    }

    @GetMapping()
    public ResponseEntity<Page> getProduct(Pageable pageable) {

        Page<Product> page = productService.getProduct(pageable);
        if (page.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(page.map(product -> mapper.map(product, ProductDTO.class)));
    }

    @GetMapping("/{code}")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable String code) {

        final Optional<Product> optionalProduct = productService.getProduct(code);
        return optionalProduct.map(product -> ResponseEntity.ok(mapper.map(product, ProductDTO.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PatchMapping("/{code}/{amount}/{cost}")
    public ResponseEntity<ProductDTO> patchProduct(@PathVariable String code, @PathVariable int amount, @PathVariable double cost) {

        final Optional<Product> optionalProduct = productService.patchProduct(code, amount, cost);
        return optionalProduct.map(product -> ResponseEntity.ok(mapper.map(product, ProductDTO.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{code}")
    public ResponseEntity deleteProduct(@PathVariable String code) {

        productService.deleteProduct(code);
        return ResponseEntity.noContent().build();
    }


    @PostMapping(value = "/upload")
    public ResponseEntity<String> uploadCSVFile(@RequestParam("email") String email, @RequestParam("file") MultipartFile file) throws IOException {

        Preconditions.checkArgument(EmailValidator.getInstance().isValid(email), "must be required valid email");

        LOGGER.info("Processing csv...");
        productService.processingUploadFile(email,  file.getOriginalFilename(), file.getInputStream());
        return ResponseEntity.ok("Processing file, when finished an email will send with summary");
    }
}
