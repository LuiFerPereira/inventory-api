package com.kometsales.inventory.contract.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ProductDTO {


    private Long id;
    private String code;
    private String productName;
    private ProductTypeDTO productType;
    private Integer amount;
    private double cost;
    private BigDecimal price;
    private LocalDate availabilityDate;

    public ProductDTO() {
    }

    public ProductDTO(String code, String productName, ProductTypeDTO productType, Integer amount, double cost, BigDecimal price,
            LocalDate availabilityDate) {
        this.code = code;
        this.productName = productName;
        this.productType = productType;
        this.amount = amount;
        this.cost = cost;
        this.price = price;
        this.availabilityDate = availabilityDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductTypeDTO getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeDTO productType) {
        this.productType = productType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", productName='" + productName + '\'' +
                ", productType=" + productType +
                ", amount=" + amount +
                ", cost=" + cost +
                ", price=" + price +
                ", availabilityDate=" + availabilityDate +
                '}';
    }
}
