package com.kometsales.inventory.domain.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BusinessException extends RuntimeException {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessException.class);

    public BusinessException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
        LOGGER.error(message, cause);
    }
}