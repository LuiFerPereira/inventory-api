package com.kometsales.inventory.domain.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class StringValueConverter extends AbstractBeanField {

    private final int MAX_LENGTH = 10;

    @Override
    protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        try {
            if (s.trim().isEmpty() || s.trim().length() > MAX_LENGTH ) {
                throw new CsvConstraintViolationException("Invalid input, must be required minimum (1 character) and maximum ("+ MAX_LENGTH +" character) length");
            }
            return s.trim();
        } catch (NumberFormatException e) {
            throw new CsvDataTypeMismatchException(e.getMessage());
        }
    }
}
