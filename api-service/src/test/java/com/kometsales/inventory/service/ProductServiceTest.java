package com.kometsales.inventory.service;

import com.kometsales.inventory.domain.model.Product;
import com.kometsales.inventory.domain.model.ProductType;
import com.kometsales.inventory.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ProductServiceTest {

    private ProductRepository productRepository = Mockito.mock(ProductRepository.class);
    private MailService mailService = Mockito.mock(MailService.class);


    private ProductService productService = new ProductService(productRepository, mailService);

    private List<Product> createMockList() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product("A1", "aName", ProductType.TYPE_2, 234, 500, LocalDate.now()));
        productList.add(new Product("B1", "otherName", ProductType.TYPE_1, 12, 25, LocalDate.now()));
        return productList;
    }

    @BeforeEach
    void setUp() {
        Mockito.clearInvocations(productRepository, mailService);
    }

    @Test
    void should_return_all_product() {

        when(productRepository.findAll(any(Pageable.class)))
                .thenReturn( new PageImpl<>(createMockList()));

        Pageable pageable = PageRequest.of(0, 20);
        final Page<Product> productPage = productService.getProduct(pageable);

        assertTrue(productPage.getTotalElements() > 0);
        verify(productRepository, times(1)).findAll(any(Pageable.class));
    }

    @Test
    void should_return_one_product() {

        when(productRepository.findByCode(anyString()))
                .thenReturn(Optional.of(new Product("A1", "aName", ProductType.TYPE_2, 234, 500, LocalDate.now())));

        final Optional<Product> optionalProduct = productService.getProduct("aCode");

        assertTrue(optionalProduct.isPresent());
        verify(productRepository, times(1)).findByCode(anyString());
    }

    @Test
    void should_delete_one_product() {

        when(productRepository.findByCode(anyString()))
                .thenReturn(Optional.of(new Product("A1", "aName", ProductType.TYPE_2, 234, 500, LocalDate.now())));

        doAnswer((invocationOnMock) -> null)
                .when(productRepository).delete(any(Product.class));

        productService.deleteProduct("aCode");

        verify(productRepository, times(1)).findByCode(anyString());
        verify(productRepository, times(1)).delete(any(Product.class));
    }

    @Test
    void should_patch_one_product() {

        when(productRepository.findByCode(anyString()))
                .thenReturn(Optional.of(new Product("A1", "aName", ProductType.TYPE_2, 234, 500, LocalDate.now())));

        when(productRepository.save(any(Product.class)))
                .thenReturn(new Product("A1", "aName", ProductType.TYPE_2, 10, 450, LocalDate.now()));

        final Optional<Product> optionalProduct = productService.patchProduct("aCode", 10, 450);

        assertTrue(optionalProduct.isPresent());
        assertEquals(10, optionalProduct.get().getAmount());
        assertEquals(450, optionalProduct.get().getCost());
        verify(productRepository, times(1)).findByCode(anyString());
    }

    @Test
    void should_processingUploadFile_with_all_rows_are_ok() throws IOException {

        doAnswer((invocationOnMock) -> null)
                .when(productRepository).save(any(Product.class));

        doAnswer((invocationOnMock) -> null)
                .when(mailService).sendEmailNotification(anyString(), anyString(), eq(4), eq(0), eq(4), anyList());

        Path resourceDirectory = Paths.get("src","test", "resources", "file_all_rows_ok.csv");
        File file = new File(resourceDirectory.toUri());
        productService.processingUploadFile("test@test.com", "file_with_grouping_same_code.csv", new FileInputStream(file));

        verify(productRepository, atLeast(1)).save(any(Product.class));
        verify(mailService, times(1)).sendEmailNotification(anyString(), anyString(), eq(4), eq(0), eq(4), anyList());
    }

    @Test
    void should_processingUploadFile_with_all_rows_are_fail() throws IOException {

        doAnswer((invocationOnMock) -> null)
                .when(productRepository).save(any(Product.class));

        doAnswer((invocationOnMock) -> null)
                .when(mailService).sendEmailNotification(anyString(), anyString(), eq(0), eq(4), eq(4), anyList());

        Path resourceDirectory = Paths.get("src","test", "resources", "file_all_rows_fail.csv");
        File file = new File(resourceDirectory.toUri());
        productService.processingUploadFile("test@test.com", "file_with_grouping_same_code.csv", new FileInputStream(file));

        verify(productRepository, times(0)).save(any(Product.class));
        verify(mailService, times(1)).sendEmailNotification(anyString(), anyString(), eq(0), eq(4), eq(4), anyList());
    }

    @Test
    void should_processingUploadFile_and_grouping_product_with_same_code() throws IOException {

        doAnswer((invocationOnMock) -> null)
                .when(productRepository).save(any(Product.class));

        doAnswer((invocationOnMock) -> null)
                .when(mailService).sendEmailNotification(anyString(), anyString(), eq(3), eq(1), eq(4), anyList());

        Path resourceDirectory = Paths.get("src","test", "resources", "file_with_grouping_same_code.csv");
        File file = new File(resourceDirectory.toUri());
        productService.processingUploadFile("test@test.com", "file_with_grouping_same_code.csv", new FileInputStream(file));

        verify(productRepository, atLeast(1)).save(any(Product.class));
        verify(mailService, times(1)).sendEmailNotification(anyString(), anyString(), eq(3), eq(1), eq(4), anyList());
    }
}