package com.kometsales.inventory.domain.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class DoubleValueConverter extends AbstractBeanField {

    private final int MAX_COST_VALUE = 99999999;

    @Override
    protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        try {
            final double value = Double.parseDouble(s.trim());
            if (value < 0 || value > MAX_COST_VALUE) {
                throw new CsvConstraintViolationException("Invalid input, must be required a number between [1,"+ MAX_COST_VALUE+"]");
            }
            return value;
        } catch (NumberFormatException e) {
            throw new CsvDataTypeMismatchException(e.getMessage());
        }
    }
}
