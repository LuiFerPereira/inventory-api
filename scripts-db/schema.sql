/* Not need run manually, the api is configured as 'create-drop' from model on startup */
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `availability_date` date DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_idx` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8