package com.kometsales.inventory.service;

import com.opencsv.exceptions.CsvException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);
    private JavaMailSender javaMailSender;

    @Autowired
    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendEmailNotification(String email, String fileName, int processedOK, int processedNotOK, int total,
            List<CsvException> errors) {

        MimeMessagePreparator mimeMessagePreparator = (MimeMessage mimeMessage) -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            message.setTo(email);
            message.setSubject("Summary processing csv file at: " + LocalDateTime.now().toString());
            message.setText(buildHtmlTemplate(fileName, processedOK, processedNotOK, total, errors), true);
        };

        javaMailSender.send(mimeMessagePreparator);
        LOGGER.info("sending email...");
    }

    private String buildHtmlTemplate(String fileName, int processedOK, int processedNotOK, int total,
            List<CsvException> errors) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html>")
                .append("<body>")
                .append("<div>")
                .append("<h1>")
                .append("Summary file processing")
                .append("</h1>")
                .append("</br>")
                .append("<h2>")
                .append("FileName:")
                .append("</h2>")
                .append(fileName)
                .append("</br>")
                .append("<h2>")
                .append("Total Product:")
                .append("</h2>")
                .append(total)
                .append("</br>")
                .append("<h2>")
                .append("Product registered successful:")
                .append("</h2>")
                .append(processedOK)
                .append("</br>")
                .append("<h2>")
                .append("Product with errors:")
                .append("</h2>")
                .append(processedNotOK)
                .append("</br>")
                .append("<h3>")
                .append("Errors:")
                .append("</h3>")
                .append("</br>")
                .append("<ul>");

        for (CsvException csvException : errors) {
            stringBuilder.append("<li>")
                    .append("Line Number:")
                    .append("&nbsp;&nbsp;")
                    .append(csvException.getLineNumber())
                    .append("&nbsp;&nbsp;-->&nbsp;&nbsp;")
                    .append(csvException.getMessage())
                    .append("</li>");
        }

        stringBuilder.append("</ul>")
                .append("</div>")
                .append("</body>")
                .append("</html>");

        return stringBuilder.toString();
    }
}
